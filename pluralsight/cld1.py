#!/usr/bin/env python
"""Retrieve and print words from URL
Usage:
	python cld1.py <URL>
"""
import sys
from urllib.request import urlopen
	
def fetch_words(url):
	"""Fetch a list of words from a URL.
	
	Args:
		url:The URL of a UTF-8 text document.
		
	Returns:
		A list of strings containing the words from the document.
	"""
	with urlopen(url) as story:
		story_words=[]
		for line in story:
			line_words=line.decode('utf-8').split()
			for word in line_words:
				story_words.append(word)
	return story_words

def print_items(items):
	"""Print items.
	Args:
		An iterable series of printable items
		"""
	for item in items:
		print(item,end=" ")

def main(url):
		"""Print each word from a text document from a URL.
		
		Args:
		url:The URL of a UTF-8 text document.
		"""
		w = fetch_words(url)
		print_items(w)

if __name__=='__main__' :
	main(sys.argv[1])	# The 0th arg is the module filename
